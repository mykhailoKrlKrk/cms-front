import "./NotificationDropdown.scss"
import DropdowmItem from "./dropdown-item/DropdownItem"

export default function NotificationDropdown(){
    const notifications = [
        {
            studName: "Bob",
            lastMessage: "Hey!What's up?",
        },
        {
            studName: "Борис",
            lastMessage: "Ти 3 лабу зробив?",
        },
        {
            studName: "Кредобанк",
            lastMessage: "ДЕ ГРОШІ?!",
        },
    ] 

    return (
        <div className="notification-dropdown">
            <ul>
                {notifications.map((notification, index) => {
                    return (
                        <DropdowmItem key={index} studName={notification.studName} lastMessage={notification.lastMessage}/>
                    )
                })}
            </ul>
        </div>
    )   
}