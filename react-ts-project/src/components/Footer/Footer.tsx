import "./Footer.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGithub, faLinkedinIn } from "@fortawesome/free-brands-svg-icons";

export default function Footer() {
  return (
    <footer className="main-footer">
      <div className="inform__icons">
        <a href="https://github.com/mykhailoKrlKrk">
          <FontAwesomeIcon className="icon__item" icon={faGithub} />
        </a>
        <a href="https://www.linkedin.com/in/mykhailo-kuryk-9ba399267/">
          <FontAwesomeIcon className="icon__item" icon={faLinkedinIn} />
        </a>
      </div>

      <div className="bottom-element">Designed by Mykhailo Kuryk</div>
    </footer>
  );
}
