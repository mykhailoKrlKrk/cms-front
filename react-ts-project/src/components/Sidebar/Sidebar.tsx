import { useState } from "react";
import "./Sidebar.scss"
import PageLinks from "../Sidebar/page-links/PageLinks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from '@fortawesome/free-solid-svg-icons';

export default function SideBar() {
  const [showSidebar, setShowSidebar] = useState(false);
  const [isButtonActive, setIsButtonActive] = useState(false);

  const toggleSidebar = () => {
    setShowSidebar(!showSidebar);
    setIsButtonActive(!isButtonActive);
  };

  return (
    <div>
      <div
        className={`dropdown-toggle ${isButtonActive ? "active" : ""}`}
        onClick={toggleSidebar}
      >
        <FontAwesomeIcon className="dropdown__item" icon={faBars} />
      </div>
      {showSidebar && (
        <aside className="sidebar">
          <PageLinks />
        </aside>
      )}
    </div>
  );
}
